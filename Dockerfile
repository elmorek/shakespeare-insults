#Base OS using Alpine.
FROM python:3.8

#Using /app as default folder.
WORKDIR /app

#Copy requirements file.
COPY requirements.txt .

#Copy environment variables.
COPY .env .

#Install all requirements
RUN pip install -r requirements.txt

#Copy all code
COPY src/ .

#Run it!
CMD ["python", "./main.py"]

