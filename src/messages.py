from insults import get_insult, get_love
import discord


def process_message(message: discord.Message) -> str:
    """
    This function filters out messages that do not contain the bot command and then gets a reply.

    :param message: instance of discord.Message class. Message sent in any channel where the bot is listening
    :return: String, returns a reply if the bot command is triggered.
    """
    content = message.content.lower()
    if is_shakespeare(content):
        return get_reply(message)


def is_shakespeare(content: str) -> bool:
    """
    This functions returns true if the boot command is used in the message content.

    :param content: Content of the message, iscord.Message.content
    :return: True if the bot command is triggered.
    """
    if 'shakespeare!' in content or 'shakespeare?' in content:
        return True
    else:
        return False


def get_first_mention(message: discord.Message) -> discord.member:
    """
    Gets the first mention in the message, if any.

    :param message: Instance of discord.Message.
    :return: Member Id of the first mention.
    """
    if len(message.mentions) >= 1:
        return message.mentions[0]


def get_reply(message: discord.Message) -> str:
    """
    Analyzes the message content and returns the required reply, either an insult or a love quote.

    :param message: Instance of discord.Message.
    :return: A string with the reply.
    """
    content = message.content.lower()
    member = get_first_mention(message)
    if 'i love you' in content:
        love = get_love()
        if member:
            reply = f'{love}, {member.mention}'
        else:
            reply = f'{love}, {message.author.mention}'
        return reply
    else:
        insult = get_insult()
        if member:
            reply = f'{member.mention}{insult}'
        else:
            reply = f'{message.author.mention}{insult}'
        return reply
