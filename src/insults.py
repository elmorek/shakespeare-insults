import math
from random import random
import json


def get_random_number(low: int, high: int) -> int:
    """
    Gets a random number between the low and high values.

    :param low: Low end of the range
    :param high: High end of the range
    :return: A random number between Low and High.
    """
    return math.floor(random()* (high - low)) + low;


def get_noun(nouns: list, index: int) -> str:
    """
    Get the noun stored in index position of the nouns list.

    :param nouns: List of nouns.
    :param index: Position required.
    :return: The requested noun.
    """
    return nouns[index]


def get_adjective(adjectives: list, index: int) -> str:
    """
    Get the adjective stored in index position of the adjectives list.

    :param adjectives: List of adjectives.
    :param index: Position required.
    :return: The requested adjective.
    """
    return adjectives[index]


def get_love_quote(love, index):
    """
   Get the love quote stored in index position of the love quotes list.

   :param love: List of love quotes.
   :param index: Position required.
   :return: The requested quote.
   """
    return love[index]


def get_insult() -> str:
    """
    Abstraction to get an insult formed by adjective and noun.

    :return: The insult reply.
    """
    with open('data/insults.json') as file:
        insults = json.load(file)
    if insults:
        noun_index = get_random_number(0, len(insults['noun']))
        adj_index = get_random_number(0, len(insults['adj']))
        noun = get_noun(insults['noun'], noun_index)
        adj = get_adjective(insults['adj'], adj_index)

        return f', thou art {adj} {noun}'


def get_love() -> str:
    """
    Abstraction to get a love quote.

    :return: A love quote.
    """
    with open('data/insults.json') as file:
        insults = json.load(file)
    if insults:
        love_quote_index = get_random_number(0, len(insults['love']))
        love_quote = get_love_quote(insults['love'], love_quote_index)
        return love_quote
