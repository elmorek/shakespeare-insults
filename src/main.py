from client import client
import os
from dotenv import load_dotenv
load_dotenv()

client.run(os.environ['token'])

