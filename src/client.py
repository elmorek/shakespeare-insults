import discord
from insults import get_insult
from messages import process_message

client = discord.Client()


@client.event
async def on_ready():
    """
    Decorator function on Discord client. Simply informs when the application is logged.
    :return: None
    """
    print(f'Logged in as {client.user}')


@client.event
async def on_message(message: discord.Message):
    """
    Decorator function on Discord client (discord.Client). Process the messages in the chat and reply, if required.
    :param message: instance of discord.Message.
    :return:
    """
    answer = process_message(message)
    if answer:
        await message.channel.send(answer)


# @client.event
# async def on_message_edit(before, after):
#     if before.content != after.content:
#         print(after.author)
